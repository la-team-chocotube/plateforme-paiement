export default {
    currencies: [
        {
            currency: "EUR",
            name: "Euros",
            sign: '€'
        },
        {
            currency: "USD",
            name: "American Dollars",
            sign: '$'
        },
        {
            currency: "GBP",
            name: "Pounds",
            sign: '£'
        },
        {
            currency: "CAD",
            name: "Canadian Dollars",
            sign: 'LOL'
        },
        {
            currency: "AUD",
            name: "Australian Dollars",
            sign: 'PTDR'
        },
    ],
    status: {
        PENDING: 'PENDING',
        CONFIRMED: 'CONFIRMED',
        CANCELED: 'CANCELED',
    }
};
