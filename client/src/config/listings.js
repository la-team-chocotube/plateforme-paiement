import Luigi from '../assets/luigi.png';
import Mario from '../assets/mario.png';
import Waluigi from '../assets/waluigi.png';
import Peach from '../assets/peach.png';
import Bowser from '../assets/bowser.png';
import Yoshi from '../assets/yoshi.png';
import Boo from '../assets/boo.png';
import Toad from '../assets/toad.png';


export default [
    {
        name: "Luigi",
        description: "He is Mario's brother, he always wear green clothes",
        price: 15,
        quantity: 0,
        image: Luigi,
    },
    {
        name: "Mario",
        description: "He is the main character and famous for is name, he used to drive often !",
        price: 27,
        quantity: 0,
        image: Mario,
    },
    {
        name: "Waluigi",
        description: "He tries to get bigger practicing bodybuilding but he's still skinny after months of musculation",
        price: 7,
        quantity: 0,
        image: Waluigi,
    },
    {
        name: "Peach",
        description: "She wants Mario to save her from Bowser, she's a prisoner in a castle surrounded by lava",
        price: 120,
        quantity: 0,
        image: Peach,
    },
    {
        name: "Bowser",
        description: "He is the gangster that frighten everyone. Even its appearance doesn't look nice !",
        price: 18,
        quantity: 0,
        image: Bowser,
    },
    {
        name: "Yoshi",
        description: "He looks like a little crocodile but he's nice with everyone don't worry he doesn't bite",
        price: 57,
        quantity: 0,
        image: Yoshi,
    },
    {
        name: "Boo",
        description: "He always tries to hide himself and forget that he's already not visible",
        price: 3,
        quantity: 0,
        image: Boo,
    },
    {
        name: "Toad",
        description: "He the cuttest mushroom ever seen in a Kart ! He's affraid of being eaten",
        price: 12,
        quantity: 0,
        image: Toad,
    },
];